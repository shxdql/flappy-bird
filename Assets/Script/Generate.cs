﻿using UnityEngine;

public class Generate : MonoBehaviour
{
    public GameObject block;

    public int score = 0;
    private int iter = 0;

    void Start()
    {
        InvokeRepeating("CreateObstacle", 1f, 1.5f);
    }

    private void OnGUI()
    {
        GUI.color = Color.white;
        GUILayout.Label("Score: " + score.ToString());
    }

    void CreateObstacle()
    {
        Instantiate(block);
        score++;
        iter++;
        if(iter > 9)
        {
            score += 10;
            iter = 0;
        }
    }
}
