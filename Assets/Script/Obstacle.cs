﻿using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [HideInInspector]
    public Vector2 vel = new Vector2(-4, 0);
    [HideInInspector]
    public float range = 4;

    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = vel;
        transform.position = new Vector3(transform.position.x,
            transform.position.y - range * Random.value, transform.position.z);
    }
}
