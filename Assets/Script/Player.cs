﻿using UnityEngine;

public class Player : MonoBehaviour
{
    public Vector2 jump = new Vector2(0, 300);

    void Update()
    {
        if(Input.GetKeyDown("space"))
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            GetComponent<Rigidbody2D>().AddForce(jump);
        }

        Vector2 screenPos = Camera.main.WorldToScreenPoint(transform.position);

        if(screenPos.y > Screen.height || screenPos.y < 0)
        {
            Die();
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Die();
    }

    void Die()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
